#ifndef PQUEUE_H
#define PQUEUE_H

#include <vector>
#include <algorithm>

using namespace std;

template <class T> struct Data
{
    T data;
    unsigned int priority;
};

template <class T> class PQueue
{
public:
    PQueue();
    PQueue(T, unsigned int);
    PQueue(const PQueue &);
    ~PQueue();
    int PQlength();
    void PQinsert(T, unsigned int);
    void PQdelete();
    void PQClear();
private:
    vector < Data <T>* > Storage;
};


template <class T> PQueue<T>::PQueue()
{
    Storage.clear();
}

template <class T> PQueue<T>::PQueue(T Var, unsigned int Pr)
{
    Data<T> * tmp = new Data<T>;
    tmp->data=Var;
    tmp->priority=Pr;
    Storage.push_back(tmp);
}

template <class T> PQueue<T>::PQueue(const PQueue & From)
{
    Storage.clear();
    for (auto iter=From.Storage.begin(); iter!=From.Storage.end(); ++iter)
        Storage.push_back(*iter);
}

template <class T> PQueue<T>::~PQueue()
{
    for(auto iter=Storage.begin(); iter!=Storage.end(); ++iter)
        delete *iter;
    Storage.clear();
}

template <class T> int PQueue<T>::PQlength()
{
    return Storage.size();
}

template <class T> void PQueue<T>::PQinsert(T Var, unsigned int Pr)
{
    Data<T> * tmp=new Data<T>;
    unsigned int CurLength=Storage.size();
    for(auto iter=Storage.begin(); iter!=Storage.end(); ++iter)
    {
        if ((*iter)->priority > Pr)
        {
            Storage.insert(iter, tmp);
            break;
        }
    }
    if (CurLength==Storage.size())
        Storage.push_back(tmp);
}
template <class T> void PQueue<T>::PQdelete()
{
    Storage.pop_back();
}

template <class T> void PQueue<T>::PQClear()
{
    for(auto iter=Storage.begin(); iter!=Storage.end(); ++iter)
        delete *iter;
    Storage.clear();
}

#endif // PQUEUE_H
