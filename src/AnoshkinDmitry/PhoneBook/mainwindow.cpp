#include "mainwindow.h"

#include "ui_mainwindow.h"

#include <QMessageBox>
#include <QString>
#include <QFileDialog>
#include <QFile>
#include <QTextStream>
#include <QMessageBox>
#include <QModelIndex>
#include <QAbstractItemView>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->adjustSize();
    model = new QStandardItemModel(1, 2, this);
    model->setHeaderData(0,Qt::Orientation::Horizontal, "Name", Qt::DisplayRole);
    model->setHeaderData(1,Qt::Orientation::Horizontal, "Number", Qt::DisplayRole);
    ui->tableView->setModel(model);
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
//    ui->tableView->setColumnWidth(0, ui->tableView->width());
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::RefreshView()
{
    model = new QStandardItemModel(1, 2, this);
    model->setHeaderData(0,Qt::Orientation::Horizontal, "Name", Qt::DisplayRole);
    model->setHeaderData(1,Qt::Orientation::Horizontal, "Number", Qt::DisplayRole);
    ui->tableView->setModel(model);
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    PBmap tmp=book.getBase();
    int row=0;
    for(auto pos=tmp.begin(); pos!=tmp.end(); ++pos){
        QModelIndex index1=model->index(row, 0, QModelIndex());
        model->setData(index1, QString::fromStdString(pos->first));
        QModelIndex index2=model->index(row, 1, QModelIndex());
        model->setData(index2, QString::fromStdString(pos->second));
        ++row;
        model->insertRow(model->rowCount());
    }
    ui->tableView->resizeColumnsToContents();
}

void MainWindow::toFile()
{
    QFile file(Filename);
    if(!file.open(QFile::WriteOnly|QFile::Text|QFile::Truncate))
    {
        QMessageBox::critical(this, tr("Error"), tr("Could not open file"));
        return;
    }
    QTextStream out(&file);
    PBmap tmp = book.getBase();
    for (auto pos=tmp.begin(); pos!=tmp.end(); ++pos)
        out<<QString::fromStdString(pos->first)<<" "<<QString::fromStdString(pos->second)<<endl;
    file.close();
}

void MainWindow::on_actionOpen_triggered()
{
    Filename=QFileDialog::getOpenFileName(this);
    if(Filename!="")
    {
        QFile file(Filename);
        if(!file.open(QFile::ReadWrite|QFile::Text))
        {
            QMessageBox::critical(this, tr("Error"), tr("Could not open file"));
            return;
        }
        QTextStream in(&file);
        std::string str;
        std::string Num="", Nam="";
        str=in.readLine().toStdString();
        while (!str.empty())
        {
            auto pos=str.begin();
            while (*pos==' ')
                ++pos;
            while (*pos!=' '){
                Num.push_back(*pos);
                ++pos;
            }
            ++pos;
            for (;pos!=str.end(); ++pos)
                Nam.push_back(*pos);
            book.addItem(Nam, Num);
            Nam.clear();
            Num.clear();
            str=in.readLine().toStdString();
        }
        RefreshView();
        file.close();
    }
}

void MainWindow::on_actionAdd_triggered()
{
    Add dialog;
    connect(&dialog, SIGNAL(add_signal(std::string,std::string)), this, SLOT(on_addSignal(std::string,std::string)));
    dialog.show();
    dialog.exec();
}

void MainWindow::on_addSignal(std::string Nam, std::string Num)
{
    QFile file(Filename);
    if(!file.open(QFile::Append|QFile::Text))
    {
        QMessageBox::critical(this, tr("Error"), tr("Could not open file"));
        return;
    }
    book.addItem(Nam, Num);
    QTextStream out(&file);
    out<<QString::fromStdString(Nam+" "+Num)<<endl;
    file.close();
    RefreshView();

}

void MainWindow::on_actionErase_triggered()
{
    QString Nam=(ui->tableView->model()->data(ui->tableView->currentIndex(), Qt::DisplayRole)).toString();
    std::string sNam=Nam.toStdString();
    book.deleteItem(sNam);
    toFile();
    RefreshView();
}

void MainWindow::on_actionFind_triggered()
{
    std::string Data=ui->leFind->text().toStdString();
    PBmap resault=book.Search(Data);
    std::string strResault="";
    if(resault.empty())
        QMessageBox::information(this, tr("Search Resault"), tr("No resault"));
    else
    {
        for(auto pos=resault.begin(); pos!=resault.end(); ++pos)
            strResault=strResault+pos->first+" - "+pos->second+ "\n";
    }
    QMessageBox::information(this, tr("Search Resault"), QString::fromStdString(strResault));
}
