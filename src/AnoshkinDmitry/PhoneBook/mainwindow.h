#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "phonebook.h"
#include "add.h"

#include <QMainWindow>
#include <QTextEdit>
#include <QStandardItemModel>
#include <string>


namespace Ui {
class MainWindow;
}

class Add;

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionOpen_triggered();

    void on_actionAdd_triggered();

    void on_addSignal(std::string, std::string);

    void on_actionErase_triggered();

    void on_actionFind_triggered();

private:
    Ui::MainWindow *ui;
    QString Filename;
    PhoneBook book;
    QStandardItemModel *model;
    void RefreshView();
    void toFile();
};

#endif // MAINWINDOW_H
