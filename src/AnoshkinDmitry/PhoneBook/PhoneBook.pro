#-------------------------------------------------
#
# Project created by QtCreator 2016-04-30T20:14:22
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PhoneBook
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    phonebook.cpp \
    add.cpp

HEADERS  += mainwindow.h \
    phonebook.h \
    add.h

FORMS    += mainwindow.ui \
    add.ui
