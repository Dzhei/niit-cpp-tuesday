#ifndef PHONEBOOK_H
#define PHONEBOOK_H

#include <map>

typedef std::multimap<std::string, std::string> PBmap;

class PhoneBook {
public:
    PhoneBook();
    PhoneBook(PBmap &);
    ~PhoneBook();

    PBmap getBase();
    void addItem(std::string Name, std::string Number);
    void addItem(PBmap &);
    void deleteItem (std::string & Name);
    PBmap Search(std::string & Data);


private:
    PBmap base;
};


#endif // PHONEBOOK_H
