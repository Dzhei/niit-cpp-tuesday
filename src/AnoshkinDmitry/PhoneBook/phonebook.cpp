#include "phonebook.h"


PhoneBook::PhoneBook(){
    base.clear();
}

PhoneBook::PhoneBook(PBmap & source)
{
    base.clear();
    for(auto pos=source.begin(); pos!=source.end(); ++pos)
        base.insert(std::pair<std::string, std::string>(pos->first, pos->second));
}

PhoneBook::~PhoneBook()
{
    base.clear();
}

PBmap PhoneBook::getBase()
{
    return base;
}

void PhoneBook::addItem(std::string Name, std::string Number)
{
    base.insert(std::pair <std::string, std::string> (Name, Number));
}

void PhoneBook::addItem(PBmap & source)
{
    for(auto pos=source.begin(); pos!=source.end(); ++pos)
        base.insert(std::pair<std::string, std::string>(pos->first, pos->second));
}
void PhoneBook::deleteItem (std::string & Name)
{
    base.erase(Name);
}

PBmap PhoneBook::Search(std::string & Data)
{
    PBmap resault;
    resault.clear();
    for(auto pos=base.begin(); pos!=base.end(); ++pos)
    {
        if (pos->first==Data || pos->second==Data)
            resault.insert(std::pair <std::string, std::string> (pos->first, pos->second));
    }
    return resault;
}

