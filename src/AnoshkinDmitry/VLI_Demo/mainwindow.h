#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "/home/dzhei/QTProjects/VLI_2/vlonginteger.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pbDevide_clicked();

    void on_pbPlus_clicked();

    void on_pbMinus_clicked();

    void on_pbMulty_clicked();

    void on_pbRest_clicked();

    void on_pbExp_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
