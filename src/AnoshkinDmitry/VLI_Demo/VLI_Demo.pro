#-------------------------------------------------
#
# Project created by QtCreator 2016-04-19T15:33:37
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = VLI_Demo
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    ../VLI_2/vlonginteger.cpp

HEADERS  += mainwindow.h \
    ../VLI_2/vlonginteger.h

FORMS    += mainwindow.ui
