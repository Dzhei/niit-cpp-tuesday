#include "automata.h"
#include <iostream>
#include <fstream>

Automata::Automata()
{
    state=OFF;
    cash=0;
    std::ifstream infile;
    infile.open("config.txt", std::ifstream::in);
    if (!infile.is_open())
            state=ERROR;
    else
    {
        int i=0;
        while(infile>>menu[i])
        {
            infile>>prices[i];
            if(prices[i]==0)
            {
                state=ERROR;
                break;
            }
            i++;
            if (i>MaxMenuSize-1)
                break;
        }
        for(;i<MaxMenuSize;i++)
        {
            menu[i]="";
            prices[i]=0;
        }
    }
}

Automata::~Automata()
{
}

bool Automata::on(){
    if (state==ERROR)
        return 0;
    state=WAIT;
//printmenu
    return 1;
}

bool Automata::off(){
    state=OFF;
    return 1;
}

int Automata::getBallance(){
    return cash;
}

std::string Automata::printMenu(int i, int &price)
{
    if (state==ERROR){
        price=0;
        return "ERROR";
    }
    if(i>MaxMenuSize || i<0)
    {
        price=0;
        return "";
    }
    else
    {
        price=prices[i];
        return menu[i];
    }
}

int Automata::coin(int Coin)
{
    if(state!=WAIT && state!=ACCEPT)
    {
        finish();       //cash out money
        return 0;
    }
    if(Coin>=0)
        cash+=Coin;
    state=ACCEPT;
    return cash;
}
    
int Automata::printState(){
    return state;
}

bool Automata::choice(int Drink)           //choice of drink
{
    if(state!=ACCEPT)
        return 0;
    state=CHECK;
    if(!check(prices[Drink])){
        state=ACCEPT;
        return 0;
    }
    cash=cash-prices[Drink];
    state=COOK;
    cook(Drink);
    return 1;
}

bool Automata::check(int price)               //Enough money?
{
    if(cash-price<0)
        return 0;
    else
        return 1;
}

bool Automata::cancel()
{
    if (cash>0)
        finish();
    if (state==ACCEPT)
        state=WAIT;
    return 1;
}

bool Automata::cook(int drink)
{
    finish();
    return 1;
}

bool Automata::finish()
{
    cash=cash%10;
    cash=cash%5;
    cash=cash%2;
    cash=0;
    state=WAIT;
    return 1;
}
