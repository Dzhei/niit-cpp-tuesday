#ifndef AUTOMATA_H
#define AUTOMATA_H

#define MaxMenuSize 12

#include <string>

enum STATES {OFF, WAIT, ACCEPT, CHECK, COOK, ERROR};

class Automata
{
private:
    STATES state;
    int cash;
    std::string menu[MaxMenuSize];
    int prices[MaxMenuSize];
    bool check(int);               //Enough money?
    bool cook(int);
    bool finish();

public:
    Automata();
    ~Automata();
    bool on();
    bool off();                 //returns cash
    int coin(int);             //deposite
    int getBallance();
    std::string printMenu(int i, int &price); //gets index and pricelink
                                              //returns price and menu string
    int printState();
    bool choice(int);          //choice of drink
    bool cancel();
};

#endif // AUTOMATA_H
