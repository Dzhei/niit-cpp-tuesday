#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QFont>
#include <QLabel>

#include "automata.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_PButton_clicked();

    void on_pushButton_clicked();

    void on_pbCoin_1_clicked();

    void on_pbCoin_2_clicked();

    void on_pbCoin_5_clicked();

    void on_pbCoin_10_clicked();

    void on_pbCoin_50_clicked();

    void on_pbCoin_100_clicked();

private:
    Ui::MainWindow *ui;
    QPushButton *pbMenu[MaxMenuSize];
    Automata CoffeM;
};

#endif // MAINWINDOW_H
