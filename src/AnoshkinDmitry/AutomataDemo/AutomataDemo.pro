#-------------------------------------------------
#
# Project created by QtCreator 2016-04-05T14:48:46
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = AutomataDemo
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    automata.cpp

HEADERS  += mainwindow.h \
    automata.h

FORMS    += mainwindow.ui
