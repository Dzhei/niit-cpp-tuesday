#include "mainwindow.h"
#include "ui_mainwindow.h"

using namespace std;

QString Status []={"OFF", "WAIT", "ACCEPT", "CHECK", "COOK", "ERROR"};

QString EditMenu(QString);

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //creates menu
    QFont *fName=new QFont();
    QFont *fPrice=new QFont();
    fName->setPointSize(12);
    fPrice->setPointSize(16);
    int i=0;
    int x=10, y=50;
    int width=100, height=100;
    int Price;
    while (QString::fromStdString(CoffeM.printMenu(i, Price)).length()){
        //creates buttons
        QString Menu=QString::fromStdString(CoffeM.printMenu(i, Price));
        Menu=EditMenu(Menu);
        pbMenu[i]=new QPushButton(this);
        pbMenu[i]->setAccessibleName(QString::number(i));
        pbMenu[i]->setGeometry(QRect(QPoint(x+(i%3)*100, y+(int)(i/3)*100), QSize(100, 100)));
        connect(pbMenu[i], SIGNAL (clicked()), this, SLOT (on_PButton_clicked()));
        pbMenu[i]->setText(Menu);
        pbMenu[i]->setFont(*fName);

        QLabel *lPrice=new QLabel(this);
        lPrice->setText(QString::number(Price));
        lPrice->setFont(*fPrice);
        lPrice->setAlignment(Qt::AlignCenter);
        lPrice->setGeometry(x+(i%3)*width+1, y+(int)(i/3)*height+height*2/3, width-2, height/3);
        i++;
    }
    CoffeM.on();
    ui->lcdSUMM->display(0);
    ui->lStateOut->setText(Status[CoffeM.printState()]);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_PButton_clicked()
{
    QPushButton * pb=(QPushButton*) sender();
    int Drink=pb->accessibleName().toInt();
    CoffeM.choice(Drink);
    ui->lStateOut->setText("COOK");
    ui->lcdSUMM->display(CoffeM.getBallance());
    ui->lStateOut->setText(Status[CoffeM.printState()]);
}


QString EditMenu(QString in){
    int i=0;
    while (i<in.length()){
        if(in[i]=='_')
            in[i]=' ';
        i++;
    }
    return in;
}

void MainWindow::on_pushButton_clicked()
{
    CoffeM.cancel();
    ui->lcdSUMM->display(CoffeM.getBallance());
    ui->lStateOut->setText(Status[CoffeM.printState()]);
}

void MainWindow::on_pbCoin_1_clicked()
{
    CoffeM.coin(1);
    ui->lcdSUMM->display(CoffeM.getBallance());
    ui->lStateOut->setText(Status[CoffeM.printState()]);
}

void MainWindow::on_pbCoin_2_clicked()
{
    CoffeM.coin(2);
    ui->lcdSUMM->display(CoffeM.getBallance());
    ui->lStateOut->setText(Status[CoffeM.printState()]);
}

void MainWindow::on_pbCoin_5_clicked()
{
    CoffeM.coin(5);
    ui->lcdSUMM->display(CoffeM.getBallance());
    ui->lStateOut->setText(Status[CoffeM.printState()]);
}

void MainWindow::on_pbCoin_10_clicked()
{
    CoffeM.coin(10);
    ui->lcdSUMM->display(CoffeM.getBallance());
    ui->lStateOut->setText(Status[CoffeM.printState()]);
}

void MainWindow::on_pbCoin_50_clicked()
{
    CoffeM.coin(50);
    ui->lcdSUMM->display(CoffeM.getBallance());
    ui->lStateOut->setText(Status[CoffeM.printState()]);
}

void MainWindow::on_pbCoin_100_clicked()
{
    CoffeM.coin(100);
    ui->lcdSUMM->display(CoffeM.getBallance());
    ui->lStateOut->setText(Status[CoffeM.printState()]);
}
