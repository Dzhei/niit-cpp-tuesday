QT += core
QT -= gui

CONFIG += c++11

TARGET = myOcean
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    ocean.cpp \
    prey.cpp \
    predatorschild.cpp \
    predators.cpp

HEADERS += \
    ocean.h \
    cell.h \
    coordinate.h \
    settings.h \
    stones.h \
    prey.h \
    predatorschild.h \
    predators.h
