#include "prey.h"

int Prey::go(Cell *Pool[RowsNumber][ColumsNumber])
{
    age++;
    if (age>=this->DeathAge)
    {
        delete this;
        return DEATH;
    }
    fromLastReproductionTime++;
    Cell * to=getFreeCell(Pool);
    if(to!=nullptr)
    {
        if(fromLastReproductionTime>=ReproductionPeriod
                && this->name!=PredatorChildName)
        {
            fromLastReproductionTime=0;
            this->Offset=to->getOffset();
            Pool[this->Offset.getX()][this->Offset.getY()]=this;
            delete to;
            return BIRTH;
        }
        Pool[to->getOffset().getX()][to->getOffset().getY()]=this;
        Pool[Offset.getX()][Offset.getY()]=to;
        Coordinate tmp=Offset;
        Offset=to->getOffset();
        to->setOffset(tmp);
        return MOVE;
    }
    else
        return PASS;
}

Cell* Prey::getFreeCell(Cell *Pool[RowsNumber][ColumsNumber])
{
    std::vector <Cell*> Directions;
    Cell * tmp;
    tmp = North(Pool);
    if (tmp->getName()==CellName)
        Directions.push_back(tmp);
    tmp=South(Pool);
    if(tmp->getName()==CellName)
        Directions.push_back(tmp);
    tmp=East(Pool);
    if(tmp->getName()==CellName)
        Directions.push_back(tmp);
    tmp=West(Pool);
    if(tmp->getName()==CellName)
        Directions.push_back(tmp);
    tmp=NorthEast(Pool);
    if(tmp->getName()==CellName)
        Directions.push_back(tmp);
    tmp=NorthWest(Pool);
    if(tmp->getName()==CellName)
        Directions.push_back(tmp);
    tmp=SouthEast(Pool);
    if(tmp->getName()==CellName)
        Directions.push_back(tmp);
    tmp=SouthWest(Pool);
    if(tmp->getName()==CellName)
        Directions.push_back(tmp);
    if (!Directions.empty())
    {
        std::random_shuffle(Directions.begin(), Directions.end());
        return Pool[Directions[0]->getOffset().getX()][Directions[0]->getOffset().getY()];
    }
    else
        return nullptr;
}

Cell * Prey::North(Cell *Pool[RowsNumber][ColumsNumber])
{
    Coordinate New;
    New.setY(Offset.getY());
    if (Offset.getX()==0)
        New.setX(RowsNumber-1);
    else
        New.setX(Offset.getX()-1);
    return Pool[New.getX()][New.getY()];
}

Cell * Prey::South(Cell *Pool[RowsNumber][ColumsNumber])
{
    return Pool[(Offset.getX()+1)%RowsNumber][Offset.getY()];
}

Cell * Prey::West(Cell *Pool[RowsNumber][ColumsNumber])
{
    Coordinate New;
    New.setX(Offset.getX());
    if (Offset.getY()==0)
        New.setY(ColumsNumber-1);
    else
        New.setY(Offset.getY()-1);
    return Pool[New.getX()][New.getY()];
}

Cell * Prey::East(Cell *Pool[RowsNumber][ColumsNumber])
{
    return Pool[Offset.getX()][(Offset.getY()+1)%ColumsNumber];
}

Cell* Prey::NorthWest(Cell *Pool[RowsNumber][ColumsNumber])
{
    int x=North(Pool)->getOffset().getX();
    int y=West(Pool)->getOffset().getY();
    return Pool[x][y];
}

Cell* Prey::SouthWest(Cell *Pool[RowsNumber][ColumsNumber])
{
    int x=South(Pool)->getOffset().getX();
    int y=West(Pool)->getOffset().getY();
    return Pool[x][y];
}

Cell* Prey::NorthEast(Cell *Pool[RowsNumber][ColumsNumber])
{
    int x=North(Pool)->getOffset().getX();
    int y=East(Pool)->getOffset().getY();
    return Pool[x][y];
}

Cell* Prey::SouthEast(Cell *Pool[RowsNumber][ColumsNumber])
{
    int x=South(Pool)->getOffset().getX();
    int y=East(Pool)->getOffset().getY();
    return Pool[x][y];
}
