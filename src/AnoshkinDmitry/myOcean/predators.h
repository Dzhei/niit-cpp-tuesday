#ifndef PREDATORS_H
#define PREDATORS_H

#include "predatorschild.h"


class Predator:public PredatorChild
{
public:
    Predator():PredatorChild(){name=PredatorName; age=PredatorGrownUpAge;}
    Predator(Coordinate Coord):
        PredatorChild(Coord){name=PredatorName; age=PredatorGrownUpAge;}

    virtual int go(Cell *Pool[RowsNumber][ColumsNumber]);
};


#endif // PREDATORS_H
