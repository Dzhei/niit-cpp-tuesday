#ifndef STONES_H
#define STONES_H

#include "cell.h"

class Stone:public Cell
{
public:
    Stone():Cell(){name=StonesName;}
    Stone(Coordinate& Coord):Cell(Coord)
                  {name=StonesName;}
    virtual ~Stone(){}
};

#endif // STONES_H
