#ifndef PREDATORSCHILD_H
#define PREDATORSCHILD_H

#include "prey.h"

class PredatorChild:public Prey
{
public:
    PredatorChild():Prey(){name=PredatorChildName;
                           DeathAge=PredatorDeathAge;
                           ReproductionPeriod=PredatorReproductionPeriod;
                           fromLastEatTime=0;}
    PredatorChild(Coordinate Coord):
        Prey(Coord){name=PredatorChildName;
                   DeathAge=PredatorDeathAge;
                   ReproductionPeriod=PredatorReproductionPeriod;
                   fromLastEatTime=0;}

    virtual int go(Cell *Pool[RowsNumber][ColumsNumber]);
    Cell* getPreyCell(Cell *Pool[RowsNumber][ColumsNumber]);
protected:
    unsigned int fromLastEatTime;
};

#endif // PREDATORSCHILD_H
