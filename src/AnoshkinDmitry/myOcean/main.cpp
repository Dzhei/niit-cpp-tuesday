#include <QCoreApplication>

#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <thread>
#include <cstring>
#include <stdlib.h>
#include <cstdio>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

#include "ocean.h"


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

/*  Сервер работает, но нет клиента пока. Проверял на чужом клиенте.
 *
 *  int sock=socket(AF_INET, SOCK_STREAM, 0);
    int sockfd;
    struct sockaddr_in addr;
    if(sock < 0)
    {
        perror("socket");
        exit(1);
    }
    addr.sin_family = AF_INET;
    addr.sin_port = htons(3333);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    int option=1;
    setsockopt(sock, SOL_SOCKET, (SO_REUSEPORT | SO_REUSEADDR), (char*)&option, sizeof(option));
    if(bind(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        perror("bind");
        exit(2);
    }
    listen(sock, 1);
    sockfd=accept(sock, NULL, NULL);
*/
    Ocean myOcean;
    int day=0;

    while(1)
    {
        char Result[1024]={0};
        system("clear");
        myOcean.OceanPrint();
        myOcean.OceanRun();
        std::cout<<"Day - "<<day<<std::endl;
        day++;
//        std::snprintf(Result, 1024, "%d\t%d\t%d\t", day, myOcean.getPredatorsCount(), myOcean.getPreyCount());
//        send(sockfd, Result , sizeof(Result), 0);
        usleep(1000*200);
    }
//    close(sock);
//    close(sockfd);
    return a.exec();
}
