#ifndef OCEAN_H
#define OCEAN_H

#include "cell.h"
#include "prey.h"

class Cell;

class Ocean
{
    friend class Cell;

public:
    Ocean();
    ~Ocean();
    void OceanRun();
    void OceanPrint();
    unsigned int getPreyCount(){return PreyCount;}
    unsigned int getPredatorsCount(){return PredatorCount;}

private:
    std::vector <Prey*> livingObjects;
    std::vector <Cell*> staticObjects;
    int PreyCount;
    int PredatorCount;
    int StonesCount;
    Cell *pPool[RowsNumber][ColumsNumber];
};

#endif // OCEAN_H
