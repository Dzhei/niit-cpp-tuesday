#ifndef CELL_H
#define CELL_H

#include <vector>
#include <algorithm>
#include <iostream>

#include "coordinate.h"
#include "settings.h"

class Cell
{
    friend class Ocean;
public:
    Cell(){name=CellName;}
    Cell(Coordinate& Coord)
        {Offset=Coord; name=CellName;}
    virtual ~Cell(){}
    void setOffset(Coordinate& Coord){Offset=Coord;}
    Coordinate & getOffset(){return Offset;}
    char getName(){return name;}

protected:
    Coordinate Offset;
    char name;
};

#endif // CELL_H
