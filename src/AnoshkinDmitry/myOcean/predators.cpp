#include "predators.h"

int Predator::go(Cell *Pool[RowsNumber][ColumsNumber])
{
    this->fromLastReproductionTime++;
    if(this->fromLastReproductionTime>PredatorReproductionPeriod)
    {
        Cell* to = getFreeCell(Pool);
        if (to!=nullptr)
        {
             fromLastReproductionTime=0;
             this->Offset=to->getOffset();
             Pool[this->Offset.getX()][this->Offset.getY()]=this;
             delete to;
             return BIRTH;
        }
        to=getPreyCell(Pool);
        {

            return KILLandBIRTH;
        }
    }
return PredatorChild::go(Pool);
}
