#include "predatorschild.h"

int PredatorChild::go(Cell *Pool[RowsNumber][ColumsNumber])
{
    this->fromLastEatTime++;
    if(this->fromLastEatTime>PredatorMaxStarvingTime)
    {
        delete this;
        return DEATH;
    }
    if (this->age>=PredatorGrownUpAge && this->name !=PredatorName)
    {
        delete this;
        return GROWUP;
    }
    Cell* to=getPreyCell(Pool);
    if(to!=nullptr)
    {
        Coordinate NewCoord=to->getOffset();
        delete to;
        Pool[NewCoord.getX()][NewCoord.getY()]=this;
        this->Offset=NewCoord;
        this->fromLastEatTime=0;
        return KILL;
    }
    return Prey::go(Pool);
}

Cell* PredatorChild::getPreyCell(Cell *Pool[RowsNumber][ColumsNumber])
{
    std::vector <Cell*> Directions;
    Cell * tmp;
    tmp = North(Pool);
    if (tmp->getName()==PreyName)
        Directions.push_back(tmp);
    tmp=South(Pool);
    if(tmp->getName()==PreyName)
        Directions.push_back(tmp);
    tmp=East(Pool);
    if(tmp->getName()==PreyName)
        Directions.push_back(tmp);
    tmp=West(Pool);
    if(tmp->getName()==PreyName)
        Directions.push_back(tmp);
    tmp=NorthEast(Pool);
    if(tmp->getName()==PreyName)
        Directions.push_back(tmp);
    tmp=NorthWest(Pool);
    if(tmp->getName()==PreyName)
        Directions.push_back(tmp);
    tmp=SouthEast(Pool);
    if(tmp->getName()==PreyName)
        Directions.push_back(tmp);
    tmp=SouthWest(Pool);
    if(tmp->getName()==PreyName)
        Directions.push_back(tmp);
    if (!Directions.empty())
    {
        std::random_shuffle(Directions.begin(), Directions.end());
        return Pool[Directions[0]->getOffset().getX()][Directions[0]->getOffset().getY()];
    }
    else
        return nullptr;
}
