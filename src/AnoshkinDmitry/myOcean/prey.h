#ifndef PREY_H
#define PREY_H

#include "cell.h"

class Prey:public Cell
{
public:
    Prey():Cell(){name=PreyName; age=0; fromLastReproductionTime=0;
                 DeathAge=PreyDeathAge; ReproductionPeriod=PreyReproductionPeriod;}
    Prey(Coordinate Coord):
        Cell(Coord){name=PreyName;  age=0; fromLastReproductionTime=0;
                   DeathAge=PreyDeathAge; ReproductionPeriod=PreyReproductionPeriod;}

    virtual int go(Cell *Pool[RowsNumber][ColumsNumber]);
    int DeathAge;
    int ReproductionPeriod;

protected:
    int age;
    int fromLastReproductionTime;

    Cell* getFreeCell(Cell *Pool[RowsNumber][ColumsNumber]);
    Cell* North(Cell *Pool[RowsNumber][ColumsNumber]);
    Cell* South(Cell *Pool[RowsNumber][ColumsNumber]);
    Cell* West(Cell *Pool[RowsNumber][ColumsNumber]);
    Cell* East(Cell *Pool[RowsNumber][ColumsNumber]);
    Cell* NorthWest(Cell *Pool[RowsNumber][ColumsNumber]);
    Cell* SouthWest(Cell *Pool[RowsNumber][ColumsNumber]);
    Cell* NorthEast(Cell *Pool[RowsNumber][ColumsNumber]);
    Cell* SouthEast(Cell *Pool[RowsNumber][ColumsNumber]);
};

#endif // PREY_H
