#ifndef COORDINATE_H
#define COORDINATE_H

class Coordinate
{
public:
    //constructors
    Coordinate(){x=0; y=0;}
    Coordinate(unsigned int xk, unsigned int yk)
                {x=xk; y=yk;}
    Coordinate(Coordinate & Coord)
                {x=Coord.x; y=Coord.y;}
    ~Coordinate(){}

    Coordinate & operator=(Coordinate & Coord)
                {x=Coord.x; y=Coord.y; return *this;}
    //interface methods
    unsigned int getX(){return x;}
    unsigned int getY(){return y;}
    void setX(unsigned int xk) {x=xk;}
    void setY(unsigned int yk) {y=yk;}

private:
    unsigned int x, y;
};

#endif // COORDINATE_H
