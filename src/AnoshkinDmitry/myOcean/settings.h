#ifndef SETTINGS_H
#define SETTINGS_H

enum results {DEATH, BIRTH, MOVE, PASS, KILL, KILLandBIRTH, GROWUP};

#define CellName '-'
#define StonesName '#'
#define PreyName 'f'
#define PredatorChildName 's'
#define PredatorName 'S'

#define ColumsNumber 50
#define RowsNumber 20

#define StonesNumber 50
#define PreyNumber 50
#define PredatorsNumber 25

#define PreyDeathAge 15
#define PredatorDeathAge 30
#define PredatorGrownUpAge 10
#define PreyReproductionPeriod 5
#define PredatorReproductionPeriod 10
#define PredatorMaxStarvingTime 5

#endif // SETTINGS_H

