#include "ocean.h"
#include "stones.h"
#include "prey.h"
#include "predators.h"
#include "predatorschild.h"


Ocean::Ocean()
{
    PreyCount=0;
    PredatorCount=0;
    StonesCount=0;

    std::vector <Cell*> objects;
    int ObjectsCount=0;

    while(ObjectsCount<StonesNumber)
    {
        objects.push_back(new Stone());
        staticObjects.push_back(objects[ObjectsCount]);
        ObjectsCount++;
        StonesCount++;
    }

    while(ObjectsCount<StonesNumber+PreyNumber)
    {
        Prey * tmp = new Prey();
        objects.push_back(tmp);
        livingObjects.push_back(tmp);
        ObjectsCount++;
        PreyCount++;
    }

    while(ObjectsCount<StonesNumber+PreyNumber+PredatorsNumber)
    {
        Predator * tmp = new Predator();
        objects.push_back(tmp);
        livingObjects.push_back(tmp);
        ObjectsCount++;
        PredatorCount++;
    }

    while(ObjectsCount<RowsNumber*ColumsNumber)
    {
        objects.push_back(new Cell());
        ObjectsCount++;
    }

    std::random_shuffle(objects.begin(), objects.end());

    for(int i=0; i<RowsNumber; i++)
        for(int j=0; j<ColumsNumber; j++){
            Coordinate Offset(i, j);
            objects[i*ColumsNumber+j]->setOffset(Offset);
            pPool[i][j]=objects[i*ColumsNumber+j];
        }
}

Ocean::~Ocean()
{

    //утечка
}

void Ocean::OceanPrint()
{
    for(int i=0; i<RowsNumber; i++){
        for(int j=0; j<ColumsNumber; j++){
            std::cout<<pPool[i][j]->name;
        }
    std::cout<<std::endl;
    }
    std::cout<<"Stones-"<<StonesCount
            <<" Prey-"<<PreyCount
           <<" Predator-"<<PredatorCount<<std::endl;
}

void Ocean::OceanRun()
{
    std::vector <Prey*> Children;
    int k=0;
    for(auto iter=livingObjects.begin(); iter!=livingObjects.end(); ++iter)
    {
        k++;
        Coordinate oldCoord=(*iter)->getOffset();
        char oldName=(*iter)->getName();
        int result = (*iter)->go(pPool);
        if (result==BIRTH)
        {
            Prey * New;
            if(oldName=='f')
            {
                New = new Prey(oldCoord);
                PreyCount++;
            }
            else
            {
                New = new PredatorChild(oldCoord);
                PredatorCount++;
            }
            pPool[oldCoord.getX()][oldCoord.getY()]=New;
            Children.push_back(New);
        }
        else if (result==DEATH)
        {
            if(oldName=='f')
                PreyCount--;
            else
                PredatorCount--;
            livingObjects.erase(iter);
            pPool[oldCoord.getX()][oldCoord.getY()]=new Cell(oldCoord);
            if (livingObjects.empty())
                break;
        }
        else if (result==KILL)
        {
            bool Found=false;
            pPool[oldCoord.getX()][oldCoord.getY()]=new Cell(oldCoord);
            PreyCount--;
            for (auto i=livingObjects.begin(); i!=livingObjects.end(); ++i)
            {
                if((*i)->getOffset().getX()==(*iter)->getOffset().getX()
                        && (*i)->getOffset().getY()==(*iter)->getOffset().getY()
                        && (*i)->getName()==PreyName)
                {
                    Found=true;
                    livingObjects.erase(i);
                    if (iter==livingObjects.end())
                        --iter;
                    break;
                }
                if(!Found)
                    for (auto i=Children.begin(); i!=Children.end(); ++i)
                        if((*i)->getOffset().getX()==(*iter)->getOffset().getX()
                                && (*i)->getOffset().getY()==(*iter)->getOffset().getY()
                                && (*i)->getName()==PreyName)
                        {
                            Children.erase(i);
                            //--iter;
                            break;
                        }
            }
        }
        else if (result==GROWUP)
        {
            Prey* New=new Predator(oldCoord);
            pPool[oldCoord.getX()][oldCoord.getY()]=New;
            Children.push_back(New);
            livingObjects.erase(iter);
        }
    }
    for(auto iter=Children.begin(); iter!=Children.end();++iter)
        livingObjects.push_back(*iter);
}
