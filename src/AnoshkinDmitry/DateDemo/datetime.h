#ifndef DATETIME_H
#define DATETIME_H
#include <ctime>
#include <cstring>


class DateTime{
public:
    DateTime();
    DateTime(int D, int M=1, int Y=1900,
             int h=0, int m=0, int s=0);
    DateTime(const DateTime& ref) {Time=ref.Time;}
    void      setDate(time_t t);
    void      setDate();
    struct tm getToday();
    struct tm getYesterday();
    struct tm getTomorrow();
    struct tm getFuture(int);
    struct tm getPast(int);
    int       getMonth();
    int       getWeekDay();
    int       calcDifference (class DateTime,
                              class DateTime);
private:
    time_t    Time;
};

#endif // DATETIME_H
