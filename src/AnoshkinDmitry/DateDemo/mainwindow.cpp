#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "datetime.h"

QString Week[7]={"Sunday","Monday", "Tuesday", "Wendsday", "Thursday", "Friday", "Saturday"};

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

DateTime date1, date2;

void MainWindow::GetInfo1()
{
    tm sTime;
    QString Year=ui->leYear1->text();
    QString Month=ui->leMonth1->text();
    QString Day=ui->leDay1->text();
    QString Hour=ui->leHour1->text();
    QString Minute=ui->leMinute1->text();
    QString Second=ui->leSecond1->text();
    if(Year.toInt())
        sTime.tm_year=Year.toInt()-1900;
    else
        sTime.tm_year=Year.toInt();
    if(Month.toInt())
        sTime.tm_mon=Month.toInt()-1;
    else
        sTime.tm_mon=Month.toInt();
    sTime.tm_mday=Day.toInt();
    sTime.tm_hour=Hour.toInt();
    sTime.tm_min=Minute.toInt();
    sTime.tm_sec=Second.toInt();
    sTime.tm_isdst=0;
    date1.setDate(mktime (&sTime));
}

void MainWindow::GetInfo2()
{
    tm sTime;
    QString Year=ui->leYear2->text();
    QString Month=ui->leMonth2->text();
    QString Day=ui->leDay2->text();
    QString Hour=ui->leHour2->text();
    QString Minute=ui->leMinute2->text();
    QString Second=ui->leSecond2->text();
    if(Year.toInt())
        sTime.tm_year=Year.toInt()-1900;
    else
        sTime.tm_year=Year.toInt();
    if(Month.toInt())
        sTime.tm_mon=Month.toInt()-1;
    else
        sTime.tm_mon=Month.toInt();
    sTime.tm_mday=Day.toInt();
    sTime.tm_hour=Hour.toInt();
    sTime.tm_min=Minute.toInt();
    sTime.tm_sec=Second.toInt();
    sTime.tm_isdst=0;
    date2.setDate(mktime (&sTime));
}


void MainWindow::PrintInfo1(tm sTime)
{
    ui->leYear1->setText(QString::number(sTime.tm_year+1900));
    ui->leMonth1->setText(QString::number(sTime.tm_mon+1));
    ui->leDay1->setText(QString::number(sTime.tm_mday));
    ui->leHour1->setText(QString::number(sTime.tm_hour));
    ui->leMinute1->setText(QString::number(sTime.tm_min));
    ui->leSecond1->setText(QString::number(sTime.tm_sec));
    on_pbGetWeekDay_clicked();
}

void MainWindow::PrintInfo2(tm sTime)
{
    ui->leYear2->setText(QString::number(sTime.tm_year+1900));
    ui->leMonth2->setText(QString::number(sTime.tm_mon+1));
    ui->leDay2->setText(QString::number(sTime.tm_mday));
    ui->leHour2->setText(QString::number(sTime.tm_hour));
    ui->leMinute2->setText(QString::number(sTime.tm_min));
    ui->leSecond2->setText(QString::number(sTime.tm_sec));
    on_pbGetWeekDay_clicked();
}

void MainWindow::on_pbPast1_clicked()
{
    GetInfo1();
    QString NDays=ui->lePastFuture1->text();
    PrintInfo1(date1.getPast(NDays.toInt()));
}

void MainWindow::on_pbPast2_clicked()
{
    GetInfo2();
    QString NDays=ui->lePastFuture2->text();
    PrintInfo2(date2.getPast(NDays.toInt()));
}

void MainWindow::on_pbToday1_clicked()
{
    PrintInfo1(date1.getToday());
}

void MainWindow::on_pbToday2_clicked()
{
    PrintInfo2(date2.getToday());
}

void MainWindow::on_pbFuture1_clicked()
{
    GetInfo1();
    QString NDays=ui->lePastFuture1->text();
    PrintInfo1(date1.getFuture(NDays.toInt()));
}

void MainWindow::on_pbFuture2_clicked()
{
    GetInfo2();
    QString NDays=ui->lePastFuture2->text();
    PrintInfo2(date2.getFuture(NDays.toInt()));
}

void MainWindow::on_pbCalc_clicked()
{
    GetInfo1();
    GetInfo2();
    int diff=date1.calcDifference(date1, date2);
    ui->lOutDiff->setText("Difference is " + QString::number(diff)+" days");
}

void MainWindow::on_pbGetWeekDay_clicked()
{
    GetInfo1();
    GetInfo2();
    QString wDay1="Day1 is " + Week[date1.getWeekDay()];
    QString wDay2="Day2 is " + Week[date2.getWeekDay()];
    ui->lWeekDayOut->setText(wDay1 + " " + wDay2);
}
