#include "circle.h"
#include <math.h>

Circle::Circle()
{
    radius=0;
    ference=0;
    square=0;
}
Circle::Circle(double R)
{
    radius=R;
    ference=2*3.14159*R;
    square=3.14159*R*R;
}

void Circle::setRadius(double R)
{
    radius=R;
    ference=2*3.14159*R;
    square=3.14159*R*R;
}

void Circle::setFerence(double F)
{
    ference=F;
    radius=F/2/3.14159;
    square=3.14159*radius*radius;
}

void Circle::setSquare(double S)
{
    square=S;
    radius=sqrt(S/3.14159);
    ference=2*3.14159*radius;
}

double Circle::GetRadius()
{
    return radius;
}

double Circle::GetFerence()
{
    return ference;
}

double Circle::GetSquare()
{
    return square;
}
