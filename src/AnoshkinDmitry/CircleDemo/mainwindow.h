#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pbRadius_clicked();

    void on_pbSqure_clicked();

    void on_pbFerence_clicked();

    void on_actionDemo_triggered();

    void on_actionEarth_triggered();

    void on_actionPool_triggered();

    void on_leLenth_cursorPositionChanged(int arg1, int arg2);

    void on_pbCalc_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
